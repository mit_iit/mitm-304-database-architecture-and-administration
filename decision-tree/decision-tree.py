import math, time
import tree_printer
import random
from multiprocessing import Process



debug = False


class Table(object):
    def __init__(self, filename=None, table=None, title=None,  attribute_name=False, separator=","):
        self.title = title
        self.filename = filename
        self.attribute_name = attribute_name
        self.separator = separator
        self.table = self.read_table() if filename else table
        if debug: self.print_table()
        super(Table, self).__init__()

    def read_table(self):
        file = open("data/{filename}".format(filename=self.filename), "r")
        lines = file.readlines()
        table = []
        for n, l in enumerate(lines):
            row = tuple(l.strip(" \t\n\r").split(self.separator))
            if n==0 and self.attribute_name:
                self.title = row
            else:
                table.append(row)
        if not self.attribute_name:
            self.title = tuple([n for n, _ in enumerate(table[0])])
        return table

    def print_table(self):
        table_format = ""
        for n, c in enumerate(self.title):
            max_len = max([len(str(c))] + [len(str(r[n])) for r in self.table])
            table_format+= "{:<%s} " % (max_len+3)
        print(table_format.format(*self.title))
        for row in self.table:
            print(table_format.format(*row))



class DecisionTree(object):
    def __init__(self, table, class_attribute, exclude_attributes):
        self.table = table
        self.class_attribute = class_attribute
        self.exclude_attributes = exclude_attributes
        self.class_frequency, self.class_entropy = self.get_entropy(attribute=class_attribute)
        self.gain, self.node = self.get_gain()
        self.tree = self.evaluate_tree()


    def evaluate_tree(self):
        tree = {self.node : dict()}
        values = self.get_column_values(column=self.node)
        for value in values:
            v = tuple(set([row[self.table.title.index(self.class_attribute)] for row in self.table.table if row[self.table.title.index(self.node)]==value]))
            if len(v)==1:
                tree[self.node][value] = v[0]
            else:
                tree[self.node][value] = self.child_node(column=self.node, value=value)

        if debug: print tree
        return tree

    def check_value(self, tree, row, title):
        key = tree.keys()[0]
        if row[title.index(key)] in  tree[key]:
            t =  tree[key][row[title.index(key)]]
        else:
            return False
        if type(t)==dict:
            return self.check_value(tree=t, row=row, title=title)
        else:
            return t

    def test_data(self, table):
        counter = 0.0
        for row in table.table:
            tree_value = self.check_value(tree=self.tree, row=row, title=table.title)
            test_value = row[table.title.index(self.class_attribute)]
            if tree_value==test_value:
                counter+=1
        accuracy = (counter/len(table.table))*100
        print "Accuracy: ", accuracy
        return accuracy



    def child_node(self, column, value):
        table, title = [], [c for c in self.table.title if c != column]

        for row in self.table.table:
            if row[self.table.title.index(column)]==value:
                new_row = [cell for n, cell in enumerate(row) if n != self.table.title.index(column)]
                table.append(new_row)
        new_table = Table(table=table, title=title)
        return DecisionTree(table=new_table, class_attribute=self.class_attribute, exclude_attributes=self.exclude_attributes).tree


    def get_gain(self):
        gain, node = {}, None
        for attribute in self.table.title:
            if attribute!=self.class_attribute and attribute not in self.exclude_attributes:
                gain[attribute] = self.class_entropy - self.get_entropy_on_attribute(attribute)
                if debug: print "Gain(\"{}\"):".format(attribute), gain[attribute], "\n"
        max_gain = max(gain.values())
        for attribute in self.table.title:
            if attribute!=self.class_attribute and attribute not in self.exclude_attributes:
                if gain[attribute]==max_gain:
                    node = attribute
                    if debug: print "Node: ", attribute
        return gain, node

    def get_entropy_on_attribute(self, attribute):
        values = self.get_column_values(attribute)
        entropy_attribute = 0.0
        for value in values:
            frequency, entropy = self.get_entropy(attribute=self.class_attribute,
                                                  cond_attribute=attribute,
                                                  cond_value=value)
            entropy_attribute += (sum(frequency.values()) / sum(self.class_frequency.values())) * entropy
        if debug: print "Entropy(\"{}\"):".format(attribute), entropy_attribute
        return entropy_attribute


    def get_column_values(self, column):
        return tuple(set([row[self.table.title.index(column)] for row in self.table.table]))


    def get_entropy(self, attribute, cond_attribute=None, cond_value=None):
        frequency = self.get_frequency(attribute=attribute, cond_attribute=cond_attribute, cond_value=cond_value)
        entropy = 0.0
        for f in frequency:
            probability = frequency[f]/sum(frequency.values())
            if probability>0:
                entropy += (-1) * probability * math.log(probability, 2)
        if debug:
            print ("Entropy:" if not cond_attribute else "IG({}-{}):".format(cond_attribute, cond_value)), entropy
        return frequency, entropy


    def get_frequency(self, attribute, cond_attribute=None, cond_value=None):
        frequency = {}
        for row in self.table.table:
            r = row[self.table.title.index(attribute)]
            if r not in frequency:
                frequency[r] = 0.0
            if not cond_attribute:
                frequency[r] += 1
            elif row[self.table.title.index(cond_attribute)]==cond_value:
                frequency[r] += 1
        if debug: print "Frequency(\"{}\"): ".format(attribute if not cond_attribute else  "{}-{}".format(cond_attribute,cond_value)), frequency
        return frequency





def decision_tree_tester(filename, class_attribute, separator=",", attribute_name=True, tree_full=False, exclude_attributes=[]):
    table = Table(filename=filename, separator=separator, attribute_name=attribute_name)

    rand_row = random.sample(range(0, len(table.table)), (len(table.table)/10)+1)
    rand_row.sort()

    test_rows, tree_rows = [], []
    for n, row in enumerate(table.table):
        if n in rand_row:
            test_rows.append(row)
        else:
            tree_rows.append(row)

    print "Tree Table:"
    tree_table = table if tree_full else Table(table=tree_rows, title=table.title)
    tree_table.print_table()

    print "Test Table:"
    test_table = Table(table=test_rows, title=table.title)
    test_table.print_table()

    decision_tree = DecisionTree(table=tree_table, class_attribute=class_attribute, exclude_attributes=exclude_attributes)

    p = Process(target=tree_printer.show_data, args=(decision_tree.tree,))
    p.start()

    return decision_tree.test_data(test_table)






if __name__ == "__main__":
    # accuracy_list = []
    #
    # for n in xrange(10):
    #     accuracy = decision_tree_tester(filename="balance-scale.txt",
    #                                     class_attribute=0,
    #                                     separator=" ",
    #                                     attribute_name=False,
    #                                     tree_full=False)
    #     accuracy_list.append(accuracy)
    # time.sleep(1)
    # print "Accuracy List:", accuracy_list

    # accuracy = decision_tree_tester(filename="tree_data.csv",
    #                                 class_attribute="Profit",
    #                                 separator=",",
    #                                 attribute_name=True,
    #                                 tree_full=True)

    accuracy = decision_tree_tester(filename="weather.csv",
                                    class_attribute="Decision",
                                    separator=",",
                                    attribute_name=True,
                                    tree_full=True,
                                    exclude_attributes=["Day"])


