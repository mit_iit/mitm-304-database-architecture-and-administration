from multiprocessing import Process
import time

"""
Handles Multiprocessing
"""


class ProcessApp(object):
    """class for process and multiprocess handle"""

    def __init__(self, **kwargs):
        """Constructor for Process"""
        super(ProcessApp, self).__init__()
        self.__dict__.update(kwargs)

    def multiprocess_limited(self, method, arg_list, max_process=10):
        """handles multiprocessing for specific method with argument list;
        keeps max number of instance within the max_process limits"""
        process_list = []
        for arg in arg_list:
            p = Process(target=method, args=(arg,))
            p.start()
            process_list.append(p)
            while len(process_list)>=max_process:
                process_list = [q for q in process_list if q.is_alive()]
                print("Process Length: {}".format(len(process_list)))
                time.sleep(1)
