import sys


class Node(object):
    def __init__(self, data, base, **kwargs):
        self.data = [None for _ in xrange(base)]

        self.base = base
        self.is_full = False
        self.__dict__.update(kwargs)

        super(Node, self).__init__(**kwargs)
        if data is not None:
            for d in data:
                self._insert_to_node(d)



    def _validate(self, data, d_type):
        if isinstance(d_type, type) and isinstance(data, d_type):
            return data
        elif isinstance(d_type, list):
            for d in d_type:
                if data is None or isinstance(data, d):
                    return data
        raise("Invalid value type.")

    def _insert_to_node(self, item):
        if not self.is_full and self._validate(item, int):
            self.data = [n for n in self.data if n!=None]
            if item not in self.data:
                self.data.append(item)
            self.data.sort()
            self.data += [None for n in xrange(self.base-len(self.data))]

        if None not in self.data:
            self.is_full = True




    def _search_in_node(self, item):
        if item in self.data:
            return dict(in_node=True, node_pos=self.data.index(item))
        else:
            for n, d in enumerate(self.data):
                if d is None:
                    return dict(in_node=False, node_pos=n)
                if item<self.data[n]:
                    return dict(in_node=False, node_pos=n)
            return dict(in_node=False, node_pos=self.base)


    def _merge_node(self, node):
        for d in node.data:
            if d is not None:
                self._insert_to_node(d)



class RootNode(Node):
    def __init__(self, children, **kwargs):
        self.children = [self._validate(child, [BranchNode, LeafNode, None]) for child in children]
        self.__dict__.update(kwargs)
        super(RootNode, self).__init__(**kwargs)
        self.children += [None for n in xrange(self.base+1-len(self.children))]


    def _merge_children(self, node, old_node):
        self._merge_node(node)
        self.children[self.children.index(old_node)] = None
        new_children = [None for _ in xrange(self.base+1)]
        for n, d in enumerate(self.data):
            if d!=None:
                for c in (self.children+node.children):
                    if c !=None:
                        if n==0:
                            if c.data[0]<d:
                                new_children[0] = c
                        if c.data[0]<=d:
                            new_children[n+1] = c
        self.children = new_children

class BranchNode(Node):
    def __init__(self, parent, children, **kwargs):
        self.parent = self._validate(parent, [RootNode, BranchNode])
        self.children = [self._validate(child, [BranchNode, LeafNode, None]) for child in children]

        self.__dict__.update(kwargs)
        super(BranchNode, self).__init__(**kwargs)
        self.children += [None for n in xrange(self.base+1-len(self.children))]

    def _merge_children(self, node, old_node):
        self._merge_node(node)
        print self.children
        self.children[self.children.index(old_node)] = None
        new_children = [None for _ in xrange(self.base+1)]
        for n, d in enumerate(self.data):
            if d!=None:
                for c in (self.children+node.children):
                    if c !=None:
                        if n==0:
                            if c.data[0]<d:
                                c.parent = self
                                new_children[0] = c
                        elif c.data[0]<=d:
                            new_children[n+1] = c
                            c.parent = self
        self.children = new_children


class LeafNode(Node):
    def __init__(self, parent, **kwargs):
        self.parent = self._validate(parent, [RootNode, BranchNode, None])

        self.__dict__.update(kwargs)
        super(LeafNode, self).__init__(**kwargs)





class BPlusTree(object):

    def __init__(self, base, **kwargs):
        self.base = base
        self.root = LeafNode(parent=None, data=None, base=self.base)

        self.__dict__.update(kwargs)
        super(BPlusTree, self).__init__(**kwargs)

    def insert(self, item):
        if isinstance(self.root, LeafNode):
            if not self.root.is_full:
                self.root._insert_to_node(item)
            else:
                self.root = self.split_leaf(self.root, item)
        else:
            cur_node = self.root
            pos = cur_node._search_in_node(item)


            while True:
                if isinstance(cur_node, LeafNode):
                    if cur_node.is_full:
                        new_node = self.split_leaf(cur_node, item)
                        print item, cur_node.parent.data
                        # print new_node.parent
                        cur_node.parent._merge_children(new_node, cur_node)
                        print item, cur_node.parent
                        # print item, cur_node.parent.data
                    else:
                        cur_node._insert_to_node(item)
                    break

                pos = cur_node._search_in_node(item)
                if not pos['in_node']:
                    cur_node = cur_node.children[pos['node_pos']]




    def split_leaf(self, node, item):
        item_list = node.data + [item]
        node1 = LeafNode(parent=None, data=item_list[:self.base-1], base=self.base)
        node2 = LeafNode(parent=None, data=item_list[-2:], base=self.base)


        new_node = RootNode(children=[node1, node2], data=[node2.data[0]], base=self.base
                            ) if node.parent is None else BranchNode(parent=None, children=[node1, node2], data=[node2.data[0]], base=self.base)
        node1.parent = self._validate_parent(new_node)
        node2.parent = self._validate_parent(new_node)

        return new_node


    def _validate_parent(self, node):
        return Node(data=None, base=self.base)._validate(data=node, d_type=[RootNode, BranchNode])

if __name__ == "__main__":
    key_list = [13, 7, 24, 15, 4, 29, 20, 16, 19, 1, 5, 22, 17]

    bt = BPlusTree(base=3)
    bt.insert(13)
    bt.insert(7)
    bt.insert(24)
    bt.insert(15)
    bt.insert(29)
    bt.insert(31)
    bt.insert(32)
    bt.insert(34)
    print bt.root.data
    print bt.root.children
    for c in bt.root.children:
        if c!=None:
            print c.data